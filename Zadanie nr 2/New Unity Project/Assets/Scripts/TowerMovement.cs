﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerMovement : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject Bullet;

    void Start()
    {
        InvokeRepeating("RotateTower", 5f, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void RotateTower (){

        int random = UnityEngine.Random.Range(15, 45);
      
        this.transform.eulerAngles += new Vector3 (0, random, 0);

        GameObject NewBullet = Instantiate(Bullet, this.transform.position, this.transform.rotation);


					NewBullet.SetActive(true);



    NewBullet.GetComponent<Rigidbody>().AddForce (transform.forward * 600);

    StartCoroutine(waiter(NewBullet));

    }

    IEnumerator waiter (GameObject Pocisk) {

        float random = UnityEngine.Random.Range(1f, 4f);
        yield return new WaitForSeconds (random);
        Destroy(Pocisk);
            
        }

    }

